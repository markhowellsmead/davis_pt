<?php

get_header();
get_template_part('partials/header');

?>
<main>
	<div class="wrapper wrapper-wide">
	<?php

	if (have_posts()) {
		get_template_part('partials/grid', 'photo');
	} else {
		get_template_part('partials/content-404');
	}

	get_template_part('partials/footer');

	?>
	</div>
</main>

<?php
get_footer();
