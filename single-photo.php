<?php

get_header();
get_template_part('partials/header');

?>
<main>
	<div class="wrapper">

		<?php if (have_posts()) :
			global $content_width;

			if (is_singular()) {
				$content_width = 1400;
			}

			while (have_posts()) :
				the_post(); ?>

				<div <?php post_class('post'); ?>>

					<?php

					if (!empty($url = get_post_meta(get_the_ID(), 'video_ref', true))) :
						the_post_video();
					elseif (has_post_thumbnail()) : ?>

						<div class="featured-image">
							<?php
	// 	                                the_post_thumbnail( has_tag( 'panorama') || has_tag( 'big-wide-world') ? 'large' : 'medium' );
								the_post_thumbnail('large');
							?>
						</div>

					<?php endif; ?>

					<?php if (! get_post_format() == 'aside') : ?>

						<h1 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a><?php edit_post_link();?></h1>

						<div class="content">

							<?php the_content(); ?>

						</div>

						<?php if (is_singular()) {
							wp_link_pages();
	                    } ?>

						<div class="meta">

							<p><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_time(get_option('date_format')); ?></a>

							<?php if (false && comments_open()) : ?>

								<span class="sep"></span><?php comments_popup_link(__('Add Comment', 'davis'), __('1 Comment', 'davis'), '% ' . __('Comments', 'davis'), '', __('Comments off', 'davis')); ?>

							<?php endif; ?>

							<?php if (is_sticky()) : ?>

								<span class="sep"></span><?php _e('Sticky', 'davis'); ?>

							<?php endif ?>

							</p>

							<?php if (is_singular('post')) : ?>

								<p><?php _e('In', 'davis'); ?> <?php the_category(', '); ?></p>
								<p><?php the_tags(' #', ' #', ' '); ?></p>

							<?php elseif (is_singular('photo')) :
								echo get_the_term_list(get_the_ID(), 'collection', '<p>#', ' #', '</p>');
							endif; ?>

						</div> <!-- .meta -->

					<?php endif; ?>

					<?php if (false && is_singular()) {
						comments_template();
	                } ?>

				</div> <!-- .post -->

				<?php
			endwhile;
		else : ?>

			<div class="post">

				<p><?php _e('Sorry, the page you requested cannot be found.', 'davis'); ?></p>

			</div> <!-- .post -->

		<?php endif; ?>

		<?php if (( ! is_singular() ) && ( $wp_query->post_count >= get_option('posts_per_page') )) : ?>

			<div class="pagination">

				<?php previous_posts_link('&larr; ' . __('Newer posts', 'davis')); ?>
				<?php next_posts_link(__('Older posts', 'davis') . ' &rarr;'); ?>

			</div> <!-- .pagination -->

		<?php endif; ?>

		<?php get_template_part('partials/footer');?>

	</div>
</main>

<?php get_footer(); ?>
