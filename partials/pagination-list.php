<?php

if ($wp_query->post_count >= get_option('posts_per_page')) {
	$pagination = the_posts_pagination([
		'mid_size' => 1,
		'prev_text' => __('Previous page', 'davis_pt'),
		'next_text' => __('Next page', 'davis_pt')
	]);
}

if (!empty($pagination)) {
	printf('<div class="pagination">%s</div>', $pagination);
}
