<?php if (is_tag() || is_tax('collection')) : ?>
	<div class="wrapper">

			<div class="summary summary-post_tag">

				<h1 class="title post-tag-title"><?php echo single_cat_title('', false);?></h1>

				<?php
				$header_description = term_description();
				if (!empty($header_description)) :?>

					<div class="post-tag-description">
						<?php echo $header_description;?>
					</div>

				<?php endif;?>

			</div>
	</div>
<?php elseif(is_search()):?>

	<div class="wrapper">

			<div class="summary summary-post_tag">

				<h1 class="title post-tag-title">Search results</h1>

				<div class="post-tag-description">
					<p>You searched for the term “<?php echo get_search_query(); ?>”.</p>
				</div>
			</div>
	</div>

<?php endif;?>

<?php if (is_month()):?>
	<div class="wrapper">
		<div class="summary">
			<?php printf( __( '<h1 class="title post-archive-monthly">Entries from %s</h1>' ), '<span>' .get_the_date(_x( 'F Y', 'monthly archives date format' )). '</span>' ); ?>
		</div>
	</div>
<?php endif;?>