<div class="wrapper wrapper-wide">
<?php
while (have_posts()) :
	the_post(); ?>

		<div <?php post_class('post'); ?>>

			<div class="grid-x align-middle">
				<div class="cell small-12 medium-7 large-6">
					<header class="gallery-header">
					<h1 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
					<?php
						the_content();
					?>
					</header>
				</div>

				<?php
				if (has_post_thumbnail()) : ?>

					<div class="cell small-12 medium-5 large-6">
						<div class="featured-image">
							<?php
								the_post_thumbnail('medium');
							?>
						</div>
					</div>
				<?php endif; ?>
			</div>

			<?php
			if (count(get_field('images'))) {
				$target_height = 300;
				echo '<div class="grid500"><!-- Grid layout origin: https://github.com/xieranmaya/blog/issues/6 #wowza -->';
				foreach (get_field('images') as $image) {
					$imagesize = @getimagesize($image['sizes']['medium']);

					if (!$imagesize) {
						$imagesize = array(
							600,
							400,
						);
					}

					// printf(
					//     '<figure class="grid-item" style="flex-grow:%s;flex-basis:%spx;">
					//         <i class="uncollapse" style="padding-bottom:%s%%"></i>
					//         <a href="%s">%s</a>
					//     </figure>',
					//     $imagesize[0] * 100 / $imagesize[1],
					//     $imagesize[0] * $target_height / $imagesize[1],
					//     $imagesize[1] / $imagesize[0] * 100,
					//     $image['sizes']['large'],
					//     '<img class="image" src="'.$image['sizes']['medium'].'" alt="'.$image['alt'].'">'
					// );

					printf(
						'<figure class="grid-item" style="flex-grow:%s;flex-basis:%spx;">
					        <i class="uncollapse" style="padding-bottom:%s%%"></i>
					        %s
					    </figure>',
						$imagesize[0] * 100 / $imagesize[1],
						$imagesize[0] * $target_height / $imagesize[1],
						$imagesize[1] / $imagesize[0] * 100,
						'<img class="image" src="'.$image['sizes']['medium'].'" alt="'.$image['alt'].'">'
					);
				}
				echo '</div>';
			}
			?>

			<?php
			wp_link_pages();
			?>

			<div class="meta">

			<p><a href="<?php echo get_month_link(get_the_time('Y'), get_the_time('m')); ?>" title="Entries from <?php the_time('F Y'); ?>"><?php the_time(get_option('date_format')); ?></a>

			<?php if (false && comments_open()) : ?>

					<span class="sep"></span><?php comments_popup_link(__('Add Comment', 'davis'), __('1 Comment', 'davis'), '% ' . __('Comments', 'davis'), '', __('Comments off', 'davis')); ?>

			<?php endif; ?>

			<?php if (is_sticky()) : ?>

					<span class="sep"></span><?php _e('Sticky', 'davis'); ?>

			<?php endif ?>

			</p>

			<p><?php the_tags(' #', ' #', ' '); ?></p>

			<?php

			if(has_tag('drone') && has_tag('switzerland')){
				echo '<p><a href="/drone/">Information about drone photography in Switzerland</a></p>';
			}
			?>
			</div>

			<?php if (false && is_singular()) {
				comments_template();
} ?>

		</div>

		<?php
endwhile;
?>
</div>
