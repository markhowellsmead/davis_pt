<div class="wrapper">
<?php

while (have_posts()) :
	the_post(); ?>

	<div <?php post_class('post'); ?>>

		<?php if (! get_post_format() == 'aside') : ?>

			<h1 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>

			<?php
				if(!empty($post->post_excerpt) && strpos($post->post_content, $post->post_excerpt) === false){
					echo '<div class="post-excerpt">' .wpautop($post->post_excerpt). '</div>';
				}
			?>

		<?php endif; ?>

		<?php

		if (!empty($url = get_post_meta(get_the_ID(), 'video_ref', true))) :
			the_post_video();
		elseif (has_post_thumbnail()) : ?>

			<div class="featured-image">
				<?php
					the_post_thumbnail('medium');
				?>
			</div>

		<?php endif; ?>

		<div class="content">

			<?php the_content(); ?>

		</div> <!-- .content -->

		<?php if (is_singular()) {
			wp_link_pages();
        } ?>

		<?php if (is_singular('post') || is_singular('photo')) : ?>

			<div class="meta">

				<p><a href="<?php echo get_month_link(get_the_time('Y'), get_the_time('m')); ?>" title="Entries from <?php the_time('F Y'); ?>"><?php the_time(get_option('date_format')); ?></a>

				<?php if (false && comments_open()) : ?>

					<span class="sep"></span><?php comments_popup_link(__('Add Comment', 'davis'), __('1 Comment', 'davis'), '% ' . __('Comments', 'davis'), '', __('Comments off', 'davis')); ?>

				<?php endif; ?>

				<?php if (is_sticky()) : ?>

					<span class="sep"></span><?php _e('Sticky', 'davis'); ?>

				<?php endif ?>

				</p>

				<?php if (is_singular('post')) : ?>

					<p><?php the_tags(' #', ' #', ' '); ?></p>
					
					<?php
					
					if(has_tag('drone') && has_tag('switzerland')){
						echo '<p><a href="/drone/">Information about drone photography in Switzerland</a></p>';
					}
					
					?>

				<?php elseif (is_singular('photo')) :
					echo get_the_term_list(get_the_ID(), 'collection', '<p>#', ' #', '</p>');
				endif; ?>

			</div> <!-- .meta -->

		<?php endif; ?>

		<?php if (false && is_singular()) {
			comments_template();
        } ?>

	</div> <!-- .post -->

	<?php
endwhile;
?>
</div>
