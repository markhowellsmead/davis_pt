<footer>

	<?php

	if (has_nav_menu('primary-menu')) {
		wp_nav_menu(array( 'theme_location' => 'primary-menu' ));
	}

	if (has_nav_menu('social-menu')) {
		wp_nav_menu(array( 'theme_location' => 'social-menu' ));
	}
	
	dynamic_sidebar( 'footer-sidebar' );

	?>

	<p>&copy; <?php echo date('Y'); ?> <a href="<?php echo esc_url(home_url()); ?>"><?php bloginfo('name'); ?></a></p>
	<p><?php _e('Theme by', 'davis'); ?> <a href="http://www.andersnoren.se">Anders Nor&eacute;n</a></p>

</footer>
