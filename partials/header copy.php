<header class="page-header">

	<div class="row">

		<p class="toggle-menu" onclick="document.querySelector('body').classList.toggle('show-menu')"><?php _e('Menu', 'davis'); ?></p>

		<?php if (has_nav_menu('primary-menu')) {
			wp_nav_menu(array( 'theme_location' => 'primary-menu' ));
	    } ?>

		<div class="site-meta">
			<h2><a href="<?php echo esc_url(home_url()); ?>"><?php bloginfo('name'); ?></a></h2>
			<p><?php bloginfo('description'); ?></p>
		</div>

	</div>

</header>

<div class="wrapper">
	<?php if (is_tag() || is_tax('collection')) : ?>

		<div class="summary summary-post_tag">

			<h1 class="title post-tag-title" style="text-transform: uppercase"><?php echo single_cat_title('', false);?></h1>

			<?php
			$header_description = term_description();
			if (!empty($header_description)) :?>

				<div class="post-tag-description">
					<?php echo $header_description;?>
				</div>

			<?php endif;?>

		</div>
	<?php endif;?>
</div>
