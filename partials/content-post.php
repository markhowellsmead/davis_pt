<div <?php post_class('post'); ?>>

	<?php
	if (has_post_video()) :
		post_video_thumbnail();
	elseif (has_post_thumbnail()) : ?>
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="featured-image">
			<?php the_post_thumbnail('medium'); ?>
		</a>
	<?php endif; ?>

	<h1 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>

	<div class="content">
		<?php the_excerpt(); ?>
	</div>

	<div class="meta">

		<p><a href="<?php echo get_month_link(get_the_time('Y'), get_the_time('m')); ?>" title="Entries from <?php the_time('F Y'); ?>"><?php the_time(get_option('date_format')); ?></a>

		<?php if (false && comments_open()) : ?>

			<span class="sep"></span><?php comments_popup_link(__('Add Comment', 'davis'), __('1 Comment', 'davis'), '% ' . __('Comments', 'davis'), '', __('Comments off', 'davis')); ?>

		<?php endif; ?>

		<?php if (is_sticky()) : ?>

			<span class="sep"></span><?php _e('Sticky', 'davis'); ?>

		<?php endif ?>

		</p>

		<?php if (is_singular('post')) : ?>

			<p><?php _e('In', 'davis'); ?> <?php the_category(', '); ?></p>
			<p><?php the_tags(' #', ' #', ' '); ?></p>

		<?php endif; ?>

	</div>

</div>
