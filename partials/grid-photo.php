<?php
if (have_posts()) :
	$target_height = 384;
?>
<div class="grid500">
	<?php
	while (have_posts()) {
		the_post();
		if (has_post_thumbnail()) {
			$directories = wp_upload_dir();

			$thumbnail_url = get_the_post_thumbnail_url(get_the_ID(), 'pt-gallery');
			if ($thumbnail_url) {
				$thumbnail_path = str_replace($directories['baseurl'], $directories['basedir'], $thumbnail_url);
				$imagesize = @getimagesize($thumbnail_path);

				if (!$imagesize) {
					$imagesize = array(
						512,
						512,
					);
				}

				printf(
					'<figure class="post grid-item">
		                <i class="uncollapse"></i>
		                <a title="%1$s" href="%2$s">%3$s<figcaption class="caption">%1$s</figcaption></a>
		            </figure>',
					get_the_title(),
					get_permalink(),
					'<img class="image" src="' . $thumbnail_url . '" alt="' . get_the_title() . '" />'
				);

				unset($thumbnail);
			}
		}
	}
	?>
</div>

<?php
	get_template_part('partials/pagination-list');
endif;
