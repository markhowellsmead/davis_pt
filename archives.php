<?php
/*
Template Name: Archives
*/

get_header();
get_template_part('partials/header');

?>

<main>
	<div class="wrapper">

		<h2 class="section-title archive-title tagcloud-title"><?php _ex('Most common topics', 'Tag cloud title', 'davis_pt');?></h2>
		<?php wp_tag_cloud();?>

		<div class="row">
			<div class="column small-12 medium-6">
				<h2 class="section-title archive-title tagcloud-title"><?php _ex('All posts', 'Tag cloud title', 'davis_pt');?></h2>
				<div class="archive-list-postbypost" style="columns: 2">
					<?php
					add_filter('the_title', 'wpse_the_title', 10, 2);
					wp_get_archives(['type' => 'postbypost']);
					remove_filter('the_title', 'wpse_the_title');
					?>
				</div>
			</div>
		</div>

		<?php get_template_part('partials/footer');?>

	</div>
</main>

<?php
get_footer();
