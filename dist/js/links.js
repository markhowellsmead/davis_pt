(function($){
	$.extend($.fn, {
		get_hostname: function(){
			var url = $(this).attr('href');
			if(url){
				url = url.toString();
				if(url.indexOf('http://')!==0 && url.indexOf('https://')!==0 && url.indexOf('//')!==0){
					return false;
				}else{
					return url.replace('http://','').replace('https://','').replace('//','').split(/[/?#]/)[0] + ''; // plus nothing to force string
				}
			}
		},
		setAsExternalLink: function(){
			$(this).each(function(){
				var $link = $(this), linkobject = this;
				var hostname = $link.get_hostname() + '';
				if(
					hostname &&
					(hostname.indexOf(window.location.hostname)<0) &&
					(hostname.indexOf(window.location.hostname.replace('www.',''))<0) && // same domain without www
					(hostname!=='') &&
					(hostname!==null) &&
					(hostname!==undefined) && (hostname!=='undefined') &&
					(hostname!==false) && (hostname!=='false') &&
					(hostname.indexOf('javascript')!==0) &&
					(hostname.indexOf('mailto:')<0) &&
					(!$link.hasClass('anchor')) &&
					(!$link.hasClass('fancybox')) &&
					(!$link.hasClass('toggle'))
				){
					this.target="_blank";
					if(this.className.indexOf("tooltip")<0){this.title=hostname;}
				}
			});
			return this;
		}
	});

	$(document).on('ready.linktarget', function(){
		$('a').setAsExternalLink();
	});

})(jQuery);