<?php

function has_post_video()
{
	return !empty(get_post_meta(get_the_ID(), 'video_ref', true));
}

function the_post_video()
{
		echo wp_oembed_get(get_post_meta(get_the_ID(), 'video_ref', true));
}

function get_post_video()
{
		return wp_oembed_get(get_post_meta(get_the_ID(), 'video_ref', true));
}

function post_video_thumbnail()
{
		echo get_post_video_thumbnail();
}


function get_post_video_thumbnail()
{
	if (empty(get_post_meta(get_the_ID(), 'video_ref', true))) {
		return '';
	}

	return empty(get_post_video_thumbnail_src(get_post_meta(get_the_ID(), 'video_ref', true))) ? '' : sprintf(
		'<a href="%s" class="post-video-thumbnail-link"><img class="post-video-thumbnail" src="%s" alt="%s" /></a>',
		get_the_permalink(), //.'?autoplay',
		get_post_video_thumbnail_src(get_post_meta(get_the_ID(), 'video_ref', true)),
		sprintf(
			'Video preview image for %s',
			get_the_title()
		)
	);
}

function get_post_video_thumbnail_src($source_url)
{

	if ($source_url == '') {
		return '';
	}

	// angabe ohne url gibt leeres string zurück
	$atts = array('url' => $source_url);

	$aPath = parse_url($atts['url']);
	$aPath['host'] = str_replace('www.', '', $aPath['host']);

	switch ($aPath['host']) {
		case 'youtu.be':
			$atts['id'] = preg_replace('~^/~', '', $aPath['path']);
			return '//i.ytimg.com/vi/'.$atts['id'].'/0.jpg';
		break;

		case 'youtube.com':
			$aParams = explode('&', $aPath['query']);
			foreach ($aParams as $param) :
				// nach parameter 'v' suchen
				$thisPair = explode('=', $param);
				if (strtolower($thisPair[0]) == 'v') :
					$atts['id'] = $thisPair[1];
					break;
				endif;
			endforeach;
			if (!$atts['id']) {
				return '';    // wenn ID nicht verfügbar, gibt leeres string zurück
			} else {
				return '//i.ytimg.com/vi/'.$atts['id'].'/0.jpg'; // gibt 1. thumbnail-bild-src zurück.
			}
			break;

		case 'vimeo.com':
			$urlParts = explode('/', $atts['url']);
			$hash = @unserialize(@file_get_contents('https://vimeo.com/api/v2/video/'.$urlParts[3].'.php'));
			if ($hash && $hash[0] && (isset($hash[0]['thumbnail_large']) && $hash[0]['thumbnail_large'] !== '')) {
				return $hash[0]['thumbnail_large'];
			} else {
				return '';//$this->template_uri.'/img/listicon_video.jpg';
			}
			break;

		default:
			// gibt leeres string zurück
			return '';
		break;
	}
}
