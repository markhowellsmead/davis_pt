<?php

namespace MarkHowellsMead\Theme\Packages;

use stdClass;

/**
 * Does groovy stuff with Photos
 *
 * @author Mark Howells-Mead <permanent.tourist@gmail.com>
 */

class Photos
{

	private $post_type = 'photo';
	private $tag_type = 'collection';

	public function run()
	{
		add_action('pre_get_posts', [$this, 'postsPerPage'], 10, 1);
		add_filter('request', [$this, 'extendFeed'], 10, 1);
		add_filter('permanenttourist-v10/post_meta_information', [$this, 'postThumbnailMeta'], 10, 2);
		add_action('mhm-attachment-from-ftp-publish/post_created', [$this, 'changePostSlug'], 10, 1);
		add_action('save_post', [$this, 'changePostSlug'], 10, 1);
		add_shortcode('pt-photo', [$this, 'shortcode'], 10, 1);

		add_action('init', [$this, 'registerCustomTaxonomies']);
		add_action('init', [$this, 'registerCustomPostType']);

		add_action('wp_enqueue_scripts', [$this, 'addScripts']);
	}

	public function registerCustomPostType()
	{
		register_post_type('photo', [
			'label' => __('Photos', 'davis_pt'),
			'labels' => [
				'name' => __('Photos', 'davis_pt'),
				'singular_name' => __('Photo', 'davis_pt'),
				'menu_name' => __('Photos', 'davis_pt'),
				'all_items' => __('All photos', 'davis_pt'),
				'add_new' => __('Add new photo', 'davis_pt'),
				'add_new_item' => __('Add new photo', 'davis_pt')
			],
			'description' => '',
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_rest' => true,
			'rest_base' => '',
			'has_archive' => true,
			'show_in_menu' => true,
			'menu_icon' => 'dashicons-camera',
			'exclude_from_search' => false,
			'capability_type' => 'post',
			'map_meta_cap' => true,
			'hierarchical' => false,
			'rewrite' => array('slug' => 'photo', 'with_front' => true),
			'query_var' => true,
			'menu_position' => 10,
			'supports' => array('title', 'editor', 'thumbnail', 'comments'),
		]);
	}

	public function registerCustomTaxonomies()
	{
		register_taxonomy('collection', ['photo'], [
			'labels' => [
				'name' => __('Collections'),
				'singular_name' => __('Collection'),
				'search_items' => __('Search Collections'),
				'all_items' => __('All Collections'),
				'parent_item' => __('Parent Collection'),
				'parent_item_colon' => __('Parent Collection:'),
				'edit_item' => __('Edit Collection'),
				'update_item' => __('Update Collection'),
				'add_new_item' => __('Add New Collection'),
				'new_item_name' => __('New Collection Name'),
				'menu_name' => __('Collection'),
			],
			'hierarchical' => false,
			'show_ui' => true,
			'show_admin_column' => true,
			'query_var' => true,
			'rewrite' => ['slug' => 'collection'],
			'show_in_rest' => true,
			'rest_base' => 'collection',
			'rest_controller_class' => 'WP_REST_Terms_Controller',
		  ]);
	}

	/**
	 * Add JavaScripts to the page.
	 */
	public function addScripts()
	{
		//wp_enqueue_script('permanenttourist-photo-collection-api', plugins_url('Resources/Public/JavaScript/collection-api.js', __FILE__), ['jquery'], 1.5, true);
	}

	private function float2rat($number, $tolerance = 1.e-6)
	{
		$holder1 = 1;
		$holder2 = 0;
		$variableK1 = 0;
		$variableK2 = 1;
		$variableB = 1 / $number;
		do {
			$variableB = 1 / $variableB;
			$variableBfloor = floor($variableB);
			$aux = $holder1;
			$holder1 = $variableBfloor * $holder1 + $holder2;
			$holder2 = $aux;
			$aux = $variableK1;
			$variableK1 = $variableBfloor * $variableK1 + $variableK2;
			$variableK2 = $aux;
			$variableB = $variableB - $variableBfloor;
		} while (abs($number - $holder1 / $variableK1) > $number * $tolerance);

		return "$holder1/$variableK1";
	}

	public function postsPerPage($query)
	{
		if (!is_admin() && $query->is_main_query() && (is_post_type_archive($this->post_type) || is_tax($this->tag_type))) {
			$query->set('posts_per_page', '24');
		}
	}

	public function postThumbnailMeta($content_meta_array, $post)
	{
		$thumbnailID = get_post_thumbnail_id($post->ID);
		$meta = wp_get_attachment_metadata($thumbnailID);

		unset($content_meta_array['publishdate']);

		if ($meta && is_array($meta['image_meta'])) {
			// Date taken
			if (isset($meta['image_meta']['created_timestamp']) && intval($meta['image_meta']['created_timestamp']) !== 0) {
				$content_meta_array['created'] = array(
					'type' => 'date-taken',
					'content' => sprintf(
						'Photographed on %1$s',
						date('j\<\s\u\p\>S\<\/\s\u\p\> F Y', $meta['image_meta']['created_timestamp'])
					),
				);
			}

			// Exposure
			if (isset($meta['image_meta']['shutter_speed']) &&
				floatval($meta['image_meta']['shutter_speed']) > 0 &&
				isset($meta['image_meta']['aperture']) &&
				floatval($meta['image_meta']['aperture']) > 0 &&
				isset($meta['image_meta']['iso']) &&
				floatval($meta['image_meta']['iso']) > 0
			) {
				if ($meta['image_meta']['shutter_speed'] > 1) {
					$shutter_speed = $meta['image_meta']['shutter_speed'];
				} else {
					$shutter_speed = $this->float2rat(floatval($meta['image_meta']['shutter_speed']));
				}

				$content_meta_array['exposure'] = array(
					'type' => 'exif',
					'content' => sprintf(
						'Exposure: %1$ss @ %2$s, ISO %3$s',
						$shutter_speed,
						'f'.$meta['image_meta']['aperture'],
						$meta['image_meta']['iso']
					),
				);
			}

			// Camera
			if (isset($meta['image_meta']['camera']) && !empty($meta['image_meta']['camera'])) {
				$content_meta_array['camera'] = array(
					'type' => 'equipment',
					'content' => sprintf(
						'Camera: %1$s',
						$meta['image_meta']['camera']
					),
				);
			}

			// Credit and copyright
			if (isset($meta['image_meta']['copyright']) && !empty($meta['image_meta']['copyright'])) {
				$content_meta_array['credit'] = array(
					'type' => 'equipment',
					'content' => sprintf(
						'Copyright %1$s',
						$meta['image_meta']['copyright']
					),
				);
			}
		}

		return $content_meta_array;
	}

	/**
	 * Add custom post type to the regular RSS feed.
	 *
	 * @param array $request The request data
	 *
	 * @return array The modified request data
	 */
	public function extendFeed($request)
	{
		if (is_array($request) && isset($request['feed']) && !isset($request['post_type'])) {
			$request['post_type'] = [
				'post',
				$this->post_type
			];
		}

		return $request;
	}

	/**
	 * Change the post slug (post_name) to the ID of the Post for this post type
	 */
	public function changePostSlug($post_id)
	{
		if (! wp_is_post_revision($post_id) && get_post_type($post_id) == $this->post_type) {
			remove_action('save_post', [$this, 'changePostSlug']);
			wp_update_post(array(
				'ID' => $post_id,
				'post_name' => (string) $post_id
			));
			add_action('save_post', [$this, 'changePostSlug'], 10, 1);
		}
	}

	public function shortcode($atts)
	{
		$atts = shortcode_atts([
			'per_page' => '30',
			'html_before' => '<div class="frp_dataroom root">',
			'html_after' => '</div>',
			'collection' => '',
		], $atts);
		if (empty($atts['collection'])) {
			return '';
		} else {
			return '<div class="mod grid500" data-pt-photo-collection="'.$atts['collection'].'" data-pt-photo-perpage="'.$atts['per_page'].'"></div>';
		}
	}
}
