<?php

namespace MarkHowellsMead\Theme\Packages;

use stdClass;
use WP_REST_Request;
use WP_REST_Response;

/**
 * Groovy stuff for the REST API
 *
 * @author Mark Howells-Mead <permanent.tourist@gmail.com>
 */

class REST
{

	public function run()
	{
		add_action('rest_api_init', [$this, 'addRestMetaFields'], 10);
		add_filter('rest_post_query', [$this, 'filterWithLocation'], 10, 2);

		// add_action('rest_api_init', [$this, 'addEndpoints'], 10);

		// This works perfectly, but isn't currently in use
		// mhm 21.7.2018
		// add_action('init', [$this, 'addFeaturedImageData'], 12);

		// add_filter( 'rest_query_vars', function( $valid_vars ) {
		// 	$valid_vars = array_merge( $valid_vars, array( 'meta_key' ) ); // Omit meta_key, meta_value if you don't need them
		// 	return $valid_vars;
		// });

	}

	public function filterWithLocation( $args, $request ) {
		$location = $request->get_param( 'withlocation' );
  		if ( ! is_null( $location ) && (strtolower($location) === 'true' || intval($location) === 1)) {
  			$args['meta_query'] = [
				'relation' => 'AND',
			  	[
					'key' => 'location',
					'compare' => 'EXISTS',
				],
				[
					'key' => 'location',
					'value' => '',
					'compare' => '!=',
				]
			];
		}
		return $args;
	}

	private function sendResponse($response_data, $response_code = 418)
	{

		$response = new WP_REST_Response($response_data);

		// Add a custom status code
		$response->set_status($response_code);

		return $response;
	}

	// public function addEndpoints() {
	// 	register_rest_route('wp/v2', '/postswithlocation/', [
	// 		'methods' => 'GET', 
	// 		'callback' => [$this, 'getPostsWithLocation']
	// 	]);
	// }

	// public function getPostsWithLocation(WP_REST_Request $request){
	// 	return $this->sendResponse(get_posts( [
	// 		'posts_per_page'   => -1,
	// 		'post_type'  => 'post',
	// 		'post_status' => 'publish',
	// 		'meta_query' => [[
	// 			'key'     => 'location',
	// 			'compare' => 'EXISTS',
	// 		]],
	// 	]), 200);
	// }

	/**
	 * Add meta data fields to the REST API responses
	 * @return void
	 */
	public function addRestMetaFields() {
		register_rest_field(['post', 'photo'], 'location', [
			'get_callback' => [$this, 'addRestMetaLocation'],
		]);
	}

	/**
	 * Adds the post location (from an ACF field) to the REST response
	 * @param array $post The post
	 */
	public function addRestMetaLocation($post){
		$location = get_post_meta( $post['id'], 'location', true);
		$return = [];
		if(is_array($location) && isset($location['lat']) && isset($location['lng'])){
			$return['latitude'] = $location['lat'];
			$return['longitude'] = $location['lng'];
			$return['address'] = isset($location['address']) ? $location['address'] : '';
		}
		return $return;
	}

	public function addFeaturedImageData()
	{
		$post_types = get_post_types(['public' => true], 'objects');
		foreach ($post_types as $post_type) {
			$post_type_name = $post_type->name;
			$show_in_rest = (bool) $post_type->show_in_rest;
			$supports_thumbnail = post_type_supports($post_type_name, 'thumbnail');
			// Only proceed if the post type is set to be accessible over the REST API
			// and supports featured images.
			if ($show_in_rest && $supports_thumbnail) {
				register_rest_field($post_type_name, 'featured_image', [
					'get_callback' => [$this, 'getFeaturedImages']
				]);
			}
		}
	}

	public function getFeaturedImages($object, $field_name, $request)
	{
		// Only proceed if the post has a featured image.
		if (!empty($object['featured_media'])) {
			$image_id = (int) $object['featured_media'];
		} elseif (!empty($object['featured_image'])) {
			// This was added for backwards compatibility with < WP REST API v2 Beta 11.
			$image_id = (int) $object['featured_image'];
		} else {
			return;
		}
		$image = get_post($image_id);
		if (!$image) {
			return;
		}
		// This is taken from WP_REST_Attachments_Controller::prepare_item_for_response().
		$featured_image['id'] = $image_id;
		$featured_image['alt_text'] = get_post_meta($image_id, '_wp_attachment_image_alt', true);
		$featured_image['caption'] = $image->post_excerpt;
		$featured_image['description'] = $image->post_content;
		$featured_image['media_type'] = wp_attachment_is_image($image_id) ? 'image' : 'file';
		$featured_image['media_details'] = wp_get_attachment_metadata($image_id);
		unset($featured_image['media_details']['file']);
		$featured_image['post'] = !empty($image->post_parent) ? (int) $image->post_parent : null;
		//$featured_image['source_url'] = wp_get_attachment_url($image_id);
		if (empty($featured_image['media_details'])) {
			$featured_image['media_details'] = new stdClass();
		} elseif (!empty($featured_image['media_details']['sizes'])) {
			//$img_url_basename = wp_basename($featured_image['source_url']);
			foreach ($featured_image['media_details']['sizes'] as $size => &$size_data) {
				$image_src = wp_get_attachment_image_src($image_id, $size);
				if (!$image_src) {
					continue;
				}
				$size_data['source_url'] = $image_src[0];
			}
		} elseif (is_string($featured_image['media_details'])) {
			// This was added to work around conflicts with plugins that cause
			// wp_get_attachment_metadata() to return a string.
			$featured_image['media_details'] = new stdClass();
			$featured_image['media_details']->sizes = new stdClass();
		} else {
			$featured_image['media_details']['sizes'] = new stdClass();
		}
		return apply_filters('featured_image', $featured_image, $image_id);
	}
}
