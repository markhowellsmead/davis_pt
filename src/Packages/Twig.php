<?php

namespace MarkHowellsMead\Theme\Packages;

use Timber\Timber;
use Timber\Image as TimberImage;
use Timber\ImageHelper as ImageHelper;

/**
 * Does groovy stuff with Twig
 *
 * @author Mark Howells-Mead <permanent.tourist@gmail.com>
 */
class Twig
{

	public function run()
	{
		add_action('timber/twig/filters', [$this, 'addFilters'], 10, 1);
		add_filter('timber_context', [$this, 'addData'], 10, 1);
		add_filter('timber/post/content/show_password_form_for_protected', [$this, 'showPasswordFormForProtectedPosts'], 10, 0);
	}

	/**
	 * Add functions to Twig
	 * @param \Twig_Environment $twig The Twig environment object
	 */
	public function addFilters($twig)
	{
		/**
		 * Shuffle (randomly mix) array entries
		 * Usage: {{ myArray|shuffle }}
		 */
		$twig->addFilter(new \Twig_SimpleFilter('shuffle', function ($array) {
			shuffle($array);
			return $array;
		}));

		/**
		 * Add SHY character
		 * Replaces ~ with &shy; so that the string is allowed to break if it needs to
		 * Usage: {{ myString|shyify }}
		 */
		$twig->addFilter(new \Twig_SimpleFilter('shyify', function ($string) {
			return str_replace('~', '&shy;', $string);
		}));

		/**
		 * Resize image to fit within width and height constraints
		 * https://github.com/timber/timber/issues/1332
		 * Usage: {{ image.src('original')|fit(600, 400) }}
		 */
		$twig->addFilter(new \Twig_SimpleFilter('fit', function ($src, $w, $h = 0) {
			// Instantiate TimberImage from $src so we have access to dimensions
			$img = new TimberImage($src);

			// If the image is smaller on both width and height, return original
			if ($img->width() <= $w && $img->height() <= $h) {
				return $src;
			}

			// Compute aspect ratio of target box
			$aspect = $w / $h;

			// Call proportional resize on width or height, depending on how the image's
			// aspect ratio compares to the target box aspect ratio
			if ($img->aspect() > $aspect) {
				return ImageHelper::resize($src, $w);
			} else {
				return ImageHelper::resize($src, 0, $h);
			}
		}));

		/**
		 * Get the size of a file and return it as a formatted string
		 * Usage: {{ image|filesize }}
		 */
		$twig->addFilter(new \Twig_SimpleFilter('filesize', function ($file) {
			if (!file_exists($file)) {
				return '';
			}

			$bytes = (int) @filesize($file);
			$decimals = 2;

			$suffixes = [
				_x(' B.', 'File size suffix'),
				_x(' Kb.', 'File size suffix'),
				_x(' Mb.', 'File size suffix'),
				_x(' Gb.', 'File size suffix'),
				_x(' Tb.', 'File size suffix'),
				_x(' Pb.', 'File size suffix')
			];

			$factor = floor((strlen($bytes) - 1) / 3);
			return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$suffixes[$factor];
		}));

		return $twig;
	}


	/**
	 * Add data to Twig
	 *
	 * @return void
	 */
	public function addData($context)
	{
		$context['queried_object'] = get_queried_object();
		return $context;
	}

	/**
	 * Returns a string including numbers in telephone format.
	 *
	 * @param mixed $number Number to be formatted.
	 * @return void
	 */
	public static function telephoneUrl($number)
	{
		$nationalprefix = '+41';
		$protocol = 'tel:';
		$formattedNumber = $number;
		if ($formattedNumber !== '') {
			// add national dialing code prefix to tel: link if it's not already set
			if (strpos($formattedNumber, '00') !== 0 && strpos($formattedNumber, '0800') !== 0 && strpos($formattedNumber, '+') !== 0 && strpos($formattedNumber, $nationalprefix) !== 0) {
				$formattedNumber = preg_replace('/^0/', $nationalprefix, $formattedNumber);
			}
		}
		$formattedNumber = str_replace('(0)', '', $formattedNumber);
		$formattedNumber = preg_replace('~[^0-9\+]~', '', $formattedNumber);
		$formattedNumber = trim($formattedNumber);
		return $protocol . $formattedNumber;
	}

	/**
	 * We need to manually define whether password-protected posts should actually be protected.
	 * To customize the form itself, use the 'timber/post/content/password_form' filter in your Theme.
	 * @return boolean       Yes please
	 */
	public function showPasswordFormForProtectedPosts()
	{
		return true;
	}
}
