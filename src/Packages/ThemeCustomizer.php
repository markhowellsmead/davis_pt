<?php
/**
 * Functionality for Customizer
 *
 * @author Mark Howells-Mead <permanent.tourist@gmail.com>
 */
namespace MarkHowellsMead\Theme\Packages;

use WP_Customize_Manager;

class ThemeCustomizer
{
	public $wp_customize = null;
	public $customizerTinyMCE = null;
	public function run()
	{
		if (class_exists('WP_Customize_Control')) {
			add_action('customize_register', [$this, 'customSections']);
		}
	}
	/**
	 * Adds a new section to use custom controls in the WordPress customiser
	 * @param  WP_Customize_Manager $this->wp_customize - WP Manager
	 *
	 * @return void
	 */
	public function customSections(WP_Customize_Manager $wp_customize)
	{
		$this->wp_customize = $wp_customize;
		$this->addFieldsText();
	}

	/**
	 * Add a field for the Google Maps API Key
	 */
	public function addGoogleMapsApiField()
	{
		if ($this->wp_customize) {
			$this->wp_customize->add_section('theme_options', [
				'title' => _x('Theme Options', 'Customizer section title', 'davis_pt'),
				'priority' => 30,
			]);
			$this->wp_customize->add_setting('google_maps_key', [
				'capability' => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
			]);
			$this->wp_customize->add_control('google_maps_key', [
				'label' => _x('Google Maps API Key', 'Theme Customizer field label', 'davis_pt'),
				'description' => _x('A key is obligatory if you want to use Google Maps on the website or in WordPress Admin.', 'Theme Customizer field description', 'davis_pt'),
				'type' => 'text',
				'section' => 'theme_options'
			]);
		}
	}
}