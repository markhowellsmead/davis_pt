<?php

namespace MarkHowellsMead\Theme;

use MarkHowellsMead\Theme\Packages\Twig;

/**
 * Theme class which gets loaded in functions.php
 * @author Mark Howells-Mead <permanent.tourist@gmail.com>
 * @version 1.0
 */
class DavisPTTheme
{

	private $theme;

	public function __construct()
	{

		$this->theme = wp_get_theme();

		$this->loadPackages([
			\MarkHowellsMead\Theme\Packages\Photos::class,
			\MarkHowellsMead\Theme\Packages\REST::class,
			\MarkHowellsMead\Theme\Packages\ThemeCustomizer::class,
			\MarkHowellsMead\Theme\Packages\Twig::class,
		]);

		/**
		 * Pass the Google Maps API key from the Theme Customizer options
		 * to the Advanced Custom Fields plugin
		 */
		add_filter('acf/fields/google_map/api', function($api){
			$api['key'] = get_theme_mod('google_maps_key', '');
			return $api;
		});

		add_action('wp_enqueue_scripts', [$this, 'registerStyles']);
		add_action('wp_enqueue_scripts', [$this, 'registerScripts']);
		add_action('admin_enqueue_scripts', [$this, 'registerAdminStyles']);
		//add_action('after_setup_theme', [$this, 'loadTranslations']);
		add_filter('document_title_parts', [$this, 'shyifyTitleTag'], 10, 1);

		add_filter('auto_update_plugin', '__return_true');
		//add_filter('max_srcset_image_width', create_function('', 'return 1;'));

		add_action('after_setup_theme', [$this, 'sizesAndFormats'], 20);
		//add_action('wp_head', [$this, 'addTypekitCode']);

		add_action('wp_print_styles', function () {
			if (! is_admin()) {
				wp_deregister_style('davis_fonts');
				wp_enqueue_style('davis_style', get_stylesheet_uri());
			}
		});

		add_filter('excerpt_more', function () {
			return sprintf(
				'�<small> (<a class="read-more" href="%1$s">%2$s</a>)</small>',
				get_permalink(get_the_ID()),
				__('Read More', 'davis')
			);
		}, 10);

		add_filter('embed_oembed_html', [$this, 'oembedFrameBorder'], 9, 1);
		add_filter('oembed_result', [$this, 'oembedFrameBorder'], 9, 1);

		add_filter('wp_head', function () {
			if (is_singular()) {
				global $post;
				echo '<meta name="created" content="' .$post->post_date. '" />'.PHP_EOL;
			}
		});


		add_action('pre_get_posts', function ($query) {
			if (!is_admin() && $query->is_main_query() && (is_post_type_archive('photo') || is_tax('collection'))) {
				$query->set('posts_per_page', '24');
			}
		});

		// add_action('mhm-attachment-from-ftp/no_file_date', function ($filepath, $exif) {
		// 	wp_mail('permanent.tourist@gmail.com', 'Import - no file date', 'Filepath '.$filepath.chr(10).'EXIF '.print_r($exif, 1));
		// });

		// add_action('mhm-attachment-from-ftp/no_valid_entries', function ($basepath, $files) {
		// 	wp_mail('permanent.tourist@gmail.com', 'Import - no valid entries', 'Basepath '.$basepath.chr(10).'Files '.print_r($files, 1));
		// });

		// add_action('mhm-attachment-from-ftp/finished', function ($entries, $processed_entries) {
		// 	wp_mail('permanent.tourist@gmail.com', 'Import - finished', 'Entries '.print_r($entries, 1).chr(10).'Processed entries '.print_r($processed_entries, 1));
		// });

		add_action('widgets_init', function () {
			register_sidebar([
				'name'          => __('Footer', 'davis'),
				'id'            => 'footer-sidebar',
				'description'   => '',
				'class'         => '',
				'before_widget' => '<div class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h2 class="widgettitle">',
				'after_title'   => '</h2>'
			]);
		});
	}

	/**
	 * Loads the provided packages.
	 *
	 * @param $packages
	 */
	private function loadPackages($packages)
	{
		foreach ($packages as $package) {
			$instance = new $package();
			$instance->run();
		}
	}

	public function sizesAndFormats()
	{
		add_image_size('pt-gallery', 512, 512, ['center, center']);
		add_image_size('post-image', 720, 9999);
		add_image_size('image-large', 1600, 1200);
		add_image_size('large', 1600, 1200);
		register_nav_menu('social-menu', __('Social Menu', 'davis'));
		add_theme_support('post-formats', ['image', 'gallery', 'video']);
	}

	// public function addTypekitCode()
	// {
		// 	echo '<script>
		//   (function(d) {
		//     var config = {
		//       kitId: "ifk0xgd",
		//       scriptTimeout: 3000,
		//       async: true
		//     },
		//     h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src="https://use.typekit.net/"+config.kitId+".js";tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
		//   })(document);
		// </script>';
	// }

	/**
	 * Remove frameborder attribute on oEmbed results for HTML5 compatability
	 */
	public function oembedFrameBorder($html)
	{
		return str_replace(' frameborder="0"', '', $html);
	}

	// public function wpse_the_title($title, $post_id)
	// {
	// 	if ($date = get_the_date('dS F Y', $post_id)) {
	// 		$title = $title. ' (' .$date. ')';
	// 	}
	// 	return $title;
	// }

	/**
	 * Load the translation files which are delivered with the Theme
	 * Other files - stored in wp-content/languages - are loaded automatically.
	 * @return void
	 */
	public function loadTranslations()
	{
		//load_theme_textdomain('davis-pt', get_stylesheet_directory_uri() . '/language');
	}

	/**
	 * Registers styles / css files for the backend.
	 * Assets are registered with the current theme version number.
	 *
	 * @return void
	 */
	public function registerAdminStyles()
	{
	}

	/**
	 * Registers styles / css files for the frontend.
	 * Assets are registered with the current theme version number.
	 *
	 * @return void
	 */
	public function registerStyles()
	{
		if (! is_user_logged_in()) {
			wp_deregister_style('dashicons');
		}

		wp_enqueue_style('davis_style', get_template_directory_uri() . '/style.css', null, '1.12.0');
		wp_enqueue_style('davis_pt_style', get_stylesheet_directory_uri() . '/style.css', array('davis_style'), $this->theme->Version);
		// wp_enqueue_style('grid500', get_stylesheet_directory_uri() . '/grid500.css', null, $this->theme->Version);
		wp_enqueue_style('assets', get_stylesheet_directory_uri() . '/assets/dist/styles/main.min.css', array('davis_pt_style'), $this->theme->Version);
		wp_enqueue_style('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
		wp_enqueue_style('playfair-display', 'https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700');
		wp_enqueue_style('pt-serif', 'https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700,700i');
		//wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Crimson+Text');
	}

	/**
	 * Registers scripts / js files for the frontend.
	 * Assets are registered with the current theme version number.
	 *
	 * @return void
	 */
	public function registerScripts()
	{
		if (file_exists(get_stylesheet_directory().'/assets/dist/scripts/main.min.js')) {
			wp_enqueue_script('main', get_stylesheet_directory_uri() . '/assets/dist/scripts/main.min.js', ['jquery'], '1.0.0', true);
		}
	}

	/**
	 * Remove tilde character - used for shyifying page or post titles - from the <TITLE> tag
	 * @param  array $titleparts The parts of the page title
	 * @return array             The potentially modified parts of the page title
	 */
	public function shyifyTitleTag($titleparts)
	{
		$titleparts['title'] = str_replace('~', '', $titleparts['title']);
		return $titleparts;
	}
}
