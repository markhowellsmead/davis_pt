<?php

use Timber\Timber;

$content_width = 1400;

$context = Timber::get_context();
$context['post'] = Timber::get_post();

Timber::render('single.twig', $context);
