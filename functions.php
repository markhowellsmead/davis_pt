<?php

namespace MarkHowellsMead\Theme;

// Dirty. Temporary solution. 2018-06-25 mhm
require_once 'src/static_functions.php';

if (!defined('WP_CLI')) {
	require_once 'vendor/autoload.php';
	$theme = new DavisPTTheme();
}

function dump($var, $die = false)
{
	echo '<pre>' .print_r($var, 1). '</pre>';
	if ($die) {
		die();
	}
}
