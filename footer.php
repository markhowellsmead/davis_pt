<?php

use Timber\Timber;
use Timber\PostQuery;

$context = Timber::get_context();

Timber::render('partials/global/toolbar.twig', $context);

?>

	<?php wp_footer(); ?>

	</body>
</html>
