<?php
/*
Template Name: Gallery page
*/

get_header();
get_template_part('partials/header');

?>

<main>
	<div class="wrapper wrapper-wide">

		<h1 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a><?php edit_post_link();?></h1>

		<?php
		if (function_exists('get_field')) {
			$images = get_field('images');

			// echo '<pre>' .print_r($images, 1). '</pre>';


			if (count($images)) {
				$target_height = 384;
				echo '<div class="gallery images ct-grid500">';
				foreach ($images as $image) {
					$imagesize = [$image['sizes']['pt-gallery-width'], $image['sizes']['pt-gallery-height']];

					printf(
						'<figure data-image="%1$s" class="grid-item" style="flex-grow:1;flex-basis:384px;">
				                <i class="uncollapse" style="padding-bottom:100%%"></i>
				                <a href="%2$s">%3$s<figcaption class="caption">%4$s</figcaption></a>
				            </figure>',
						$image['ID'],
						'#', //get_permalink(),
						'<img class="image" src="' . $image['sizes']['pt-gallery'] . '" alt="' . (!empty($image['alt']) ? $image['alt'] : $image['title']) . '" />',
						!empty($image['alt']) ? $image['alt'] : $image['title']
					);



					//printf('<figure class="gallery imageholder grid-item" style="background-image:url(\'%1$s\')"><img class="gallery image" src="%1$s" alt="%2$s" /></figure>', $image['sizes']['pt-gallery'], $image['alt']);
				}
				echo '</div>';
			}
		}
			get_template_part('partials/footer');
		?>

	</div>
</main>
<?php get_footer(); ?>
