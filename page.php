<?php

get_header();
get_template_part('partials/header');

?>
<main>
	<div class="wrapper">

		<?php if (have_posts()) :
			while (have_posts()) :
				the_post(); ?>

				<div <?php post_class('post'); ?>>

					<?php if (! get_post_format() == 'aside') : ?>

						<h1 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>

					<?php endif; ?>

					<?php if (has_post_thumbnail()) : ?>

						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="featured-image">
							<?php the_post_thumbnail('medium'); ?>
						</a>

					<?php endif; ?>

					<div class="content">

						<?php the_content(); ?>

					</div> <!-- .content -->

					<?php if (is_singular()) {
						wp_link_pages();
	                } ?>

					<?php if (get_post_type() == 'post') : ?>

						<div class="meta">

							<p><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_time(get_option('date_format')); ?></a>

							<?php if (comments_open()) : ?>

								<span class="sep"></span><?php comments_popup_link(__('Add Comment', 'davis'), __('1 Comment', 'davis'), '% ' . __('Comments', 'davis'), '', __('Comments off', 'davis')); ?>

							<?php endif; ?>

							<?php if (is_sticky()) : ?>

								<span class="sep"></span><?php _e('Sticky', 'davis'); ?>

							<?php endif ?>

							</p>

							<?php if (is_singular('post')) : ?>

								<p><?php _e('In', 'davis'); ?> <?php the_category(', '); ?></p>
								<p><?php the_tags(' #', ' #', ' '); ?></p>

							<?php endif; ?>

						</div> <!-- .meta -->

					<?php endif; ?>

					<?php if (is_singular()) {
						comments_template();
	                } ?>

				</div> <!-- .post -->

				<?php
			endwhile;
		else : ?>

			<div class="post">

				<p><?php _e('Sorry, the page you requested cannot be found.', 'davis'); ?></p>

			</div> <!-- .post -->

		<?php endif; ?>

		<?php if (( ! is_singular() ) && ( $wp_query->post_count >= get_option('posts_per_page') )) : ?>

			<div class="pagination">

				<?php previous_posts_link('&larr; ' . __('Newer posts', 'davis')); ?>
				<?php next_posts_link(__('Older posts', 'davis') . ' &rarr;'); ?>

			</div> <!-- .pagination -->

		<?php endif; ?>

		<?php get_template_part('partials/footer');?>

	</div> <!-- .wrapper -->
</main>

<?php get_footer(); ?>
